package com.faceview.abservetech.faceview.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.faceview.abservetech.faceview.ImagePath;

import com.faceview.abservetech.faceview.SelectHairStyle;
import com.faceview.abservetech.faceview.SelectHairstyle2;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
ImagePath imagePath;
    public ImageAdapter(Context c ) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(300,300));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);



        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("position---",""+position);

                imagePath.setPosition(position);
                Intent i= new Intent(mContext,SelectHairStyle.class);

                SelectHairStyle.position=position;
                Intent i= new Intent(mContext,SelectHairstyle2.class);

                mContext.startActivity(i);

            }
        });

        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.modellook11, R.drawable.modellook22,
            R.drawable.modellook33, R.drawable.modellook44,

    };
}