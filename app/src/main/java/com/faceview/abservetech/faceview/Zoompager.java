package com.faceview.abservetech.faceview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.media.FaceDetector;
import android.os.Build;
import android.os.Bundle;


import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;


import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;

import java.util.ArrayList;
import java.util.List;

public class Zoompager extends AppCompatActivity {


    ImageView img, eyeline, sholder, waist, knee, ankle;
    int pointnum = 1;
    private Paint mPaint;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new SingleTouchEventView(this));
    }

    public class SingleTouchEventView extends View {
        List<Point> points = new ArrayList<Point>();
        float myEyesDistance;
        int numberOfFaceDetected;
        Bitmap myBitmap;
        private Paint paint = new Paint();
        private Paint paint1 = new Paint();
        private int imageWidth, imageHeight;
        private int numberOfFace = 5;
        private FaceDetector myFaceDetect;
        private FaceDetector.Face[] myFace;
        SparseArray<Face> faceArray;

        public SingleTouchEventView(Context context) {
            super(context);

            BitmapFactory.Options BitmapFactoryOptionsbfo = new BitmapFactory.Options();
            BitmapFactoryOptionsbfo.inPreferredConfig = Bitmap.Config.RGB_565;

            myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.check, BitmapFactoryOptionsbfo);

            myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.male_model, BitmapFactoryOptionsbfo);

            imageWidth = myBitmap.getWidth();
            imageHeight = myBitmap.getHeight();
            myFace = new FaceDetector.Face[numberOfFace];
            myFaceDetect = new FaceDetector(imageWidth, imageHeight, numberOfFace);
            numberOfFaceDetected = myFaceDetect.findFaces(myBitmap, myFace);

            com.google.android.gms.vision.face.FaceDetector detector = new com.google.android.gms.vision.face.FaceDetector.Builder(Zoompager.this)
                    .setMode(com.google.android.gms.vision.face.FaceDetector.ACCURATE_MODE)
                    .setLandmarkType(com.google.android.gms.vision.face.FaceDetector.ALL_LANDMARKS)
                    .setClassificationType(com.google.android.gms.vision.face.FaceDetector.ALL_CLASSIFICATIONS)
                    .setTrackingEnabled(false).build();
            Frame frame = new Frame.Builder().setBitmap(myBitmap).build();
            faceArray = detector.detect(frame);
        }

        protected void onDraw(Canvas canvas) {

            super.onDraw(canvas);


            canvas.drawBitmap(myBitmap, 0, 0, null);

            Paint myPaint = new Paint();
            myPaint.setColor(Color.GREEN);


            for (int i = 0; i < numberOfFaceDetected; i++) {
                FaceDetector.Face face = myFace[i];
                PointF myMidPoint = new PointF();
                face.getMidPoint(myMidPoint);

                myEyesDistance = face.eyesDistance();
                canvas.drawCircle((int) (myMidPoint.x), (int) (myMidPoint.y), 8, myPaint);
                canvas.drawCircle((int) (myMidPoint.x)+50, (int) (myMidPoint.y)+20, 8, myPaint);
                canvas.drawCircle((int) (myMidPoint.x)-50, (int) (myMidPoint.y)+20, 8, myPaint);
                canvas.drawCircle((int) (myMidPoint.x), (int) (myMidPoint.y)+100, 8, myPaint);
                canvas.drawCircle((int) (myMidPoint.x), (int) (myMidPoint.y)+50, 8, myPaint);
                canvas.drawCircle((int) (myMidPoint.x)+100, (int) (myMidPoint.y)+100, 8, myPaint);
                canvas.drawCircle((int) (myMidPoint.x)-100, (int) (myMidPoint.y)+100, 8, myPaint);
                // canvas.drawText(String.valueOf(pointnum),(int)(myMidPoint.x ),(int)(myMidPoint.y ) ,paint1);

            }
            pointnum++;
            for(int i=0; i < faceArray.size(); i++) {
                Face face = faceArray.get(i);

                //Drawing rectangle on each face
//                drawRectangle(canvas, face.getPosition(), face.getWidth(), face.getHeight());

                //Drawing a point on each face features
                for (Landmark landmark : face.getLandmarks()) {
                    switch (landmark.getType()) {

                        case Landmark.LEFT_EAR:
                            drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.LEFT_EAR_TIP:
                            drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.RIGHT_EAR:
                            drawPoint(canvas, landmark.getPosition());
                            break;
                        case Landmark.RIGHT_EAR_TIP:
                            drawPoint(canvas, landmark.getPosition());
                            break;

                    }
                }
            }
            paint.setColor(Color.GREEN);
            paint1.setColor(Color.BLACK);
            for (Point p : points) {
                canvas.drawCircle(p.x, p.y, 10, paint);
                //canvas.drawText(String.valueOf(pointnum),p.x,p.y ,paint1);
                pointnum++;
            }

            invalidate();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:

                    Point p = new Point();
                    p.x = (int) event.getX();
                    p.y = (int) event.getY();
                    points.add(p);
                    invalidate();

                case MotionEvent.ACTION_MOVE:
//                    Point p1 = new Point();
//                    p1.x = (int) event.getX();
//                    p1.y = (int) event.getY();
//                    points.add(p1);
//                    invalidate();
                case MotionEvent.ACTION_UP:
//                    Point p2 = new Point();
//                    p2.x = (int) event.getX();
//                    p2.y = (int) event.getY();
//                    points.add(p2);
//                    invalidate();
                case MotionEvent.ACTION_CANCEL: {
                    break;
                }
            }
            invalidate();
            return true;

        }


    }
    private void drawRectangle(Canvas canvas, PointF point, float width,
                               float height){
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);

        float x1 = point.x;
        float y1 = point.y;
        float x2 = x1 + width;
        float y2 = y1 + height;

        RectF rect = new RectF(x1, y1, x2, y2);
        canvas.drawRect(rect, paint);
    }


    //This method draws a point with hole
    private void drawPoint(Canvas canvas, PointF point){
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(8);
        paint.setStyle(Paint.Style.STROKE);

        float x = point.x;
        float y = point.y;

        canvas.drawCircle(x, y, 1, paint);
    }
}