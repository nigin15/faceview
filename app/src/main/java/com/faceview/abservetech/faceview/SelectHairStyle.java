package com.faceview.abservetech.faceview;


import android.annotation.TargetApi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;

import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.google.android.gms.vision.face.FaceDetector;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import android.content.Context;

import android.util.SparseArray;
import android.widget.Toast;


import com.faceview.abservetech.faceview.Adapter.MemoryCache;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;

import java.util.ArrayList;
import java.util.List;




    public class SelectHairStyle extends AppCompatActivity implements View.OnClickListener {


        public static int position;
        ImagePath imagePath;
        RelativeLayout relativeLayout;
        ImageView female, male, long_type, medium_type, short_type, classic_type, moedern_type;
        LinearLayout malehair, femalehair, medium_hair, long_hair, short_hair, classic_hair, modern_hair;
        Bitmap myBitmap;


        private Boolean modern_t=false;
        private Boolean classic_t=false;
        private ImageView fl1,fl2,fl3,fl4,fl5,fl6,fl7,fl8,fl9,fl10,
                fl11,fl12,fl13,fl14,fl15,fl16,fl17,image;
        private ImageView fm1,fm2,fm3,fm4,fm5,fm6,fm7,fm8,fm9,fm10,
                fm11,fm12,fm13,fm14,fm15,fm16,fm17,fm18,fm19,fm20,
                fm21,fm22,fm23,fm24,fm25,fm26,fm27,fm28,fm29,fm30;

        private ImageView mc1,mc2,mc3,mc4,mc5,mc6,mc7,mc8,mc9,mc10,
                mc11,mc12,mc13,mc14,mc15,mc16,mc17,mc18,mc19,mc20
                ;
        private ImageView fs1,fs2,fs3,fs4,fs5,fs6,fs7,fs8,fs9,fs10,
                fs11,fs12,fs13,fs14,fs15,fs16,fs17,fs18,fs19,fs20,fs21;


        int pointnum = 1;
        private Boolean short_t = false;
        private Boolean medium_t = false;
        private Boolean long_t = false;
        private Boolean side = false;
        private Boolean modern_t = false;
        private Boolean classic_t = false;
        private ImageView fl1, fl2, fl3, fl4, fl5, fl6, fl7, fl8, fl9, fl10,
                fl11, fl12, fl13, fl14, fl15, fl16, fl17, image;
        private ImageView fm1, fm2, fm3, fm4, fm5, fm6, fm7, fm8, fm9, fm10,
                fm11, fm12, fm13, fm14, fm15, fm16, fm17, fm18, fm19, fm20,
                fm21, fm22, fm23, fm24, fm25, fm26, fm27, fm28, fm29, fm30;
        private ImageView mc1, mc2, mc3, mc4, mc5, mc6, mc7, mc8, mc9, mc10,
                mc11, mc12, mc13, mc14, mc15, mc16, mc17, mc18, mc19, mc20;
        private ImageView mm1, mm2, mm3, mm4, mm5, mm6, mm7, mm8, mm9, mm10,
                mm11, mm12, mm13, mm14, mm15, mm16, mm17, mm18, mm19, mm20,
                mm21, mm22, mm23, mm24, mm25;
        private View.OnClickListener femaleclick = new View.OnClickListener() {
            public void onClick(View v) {
                // Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(SelectHairStyle.this, R.anim.slideout);
                //  relativeLayout.startAnimation(hyperspaceJumpAnimation);
                relativeLayout.setVisibility(View.GONE);
                femalehair.setVisibility(View.VISIBLE);
                malehair.setVisibility(View.GONE);
                female.setBackgroundResource(R.drawable.female1);
                male.setBackgroundResource(R.drawable.male);
            }
        };
        private View.OnClickListener maleclick = new View.OnClickListener() {
            public void onClick(View v) {
                //  Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(SelectHairStyle.this, R.anim.slideout);
                // relativeLayout.startAnimation(hyperspaceJumpAnimation);
                relativeLayout.setVisibility(View.GONE);
                femalehair.setVisibility(View.GONE);
                malehair.setVisibility(View.VISIBLE);
                female.setBackgroundResource(R.drawable.female);
                male.setBackgroundResource(R.drawable.male1);
            }
        };


        private Paint rectPaint;
        private Bitmap defaultBitmap;
        private Bitmap temporaryBitmap;
        private Bitmap eyePatchBitmap;
        private Canvas canvas;


        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_select_hairstyle);
            relativeLayout = (RelativeLayout) findViewById(R.id.sidemenulayout);
            image = (ImageView) findViewById(R.id.image);
            female = (ImageView) findViewById(R.id.female);
            male = (ImageView) findViewById(R.id.male);
            long_type = (ImageView) findViewById(R.id.long_type);
            short_type = (ImageView) findViewById(R.id.short_type);
            medium_type = (ImageView) findViewById(R.id.medium_type);
            classic_type = (ImageView) findViewById(R.id.classic_type);
            moedern_type = (ImageView) findViewById(R.id.moedern_type);

            malehair = (LinearLayout) findViewById(R.id.malehair);
            femalehair = (LinearLayout) findViewById(R.id.femalehair);

            medium_hair = (LinearLayout) findViewById(R.id.medium_hair);
            long_hair = (LinearLayout) findViewById(R.id.long_hair);
            short_hair = (LinearLayout) findViewById(R.id.short_hair);
            classic_hair = (LinearLayout) findViewById(R.id.classic_hair);
            modern_hair = (LinearLayout) findViewById(R.id.modern_hair);
            Initializer();
            imageconversion();
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int height = displaymetrics.heightPixels;
            int width = displaymetrics.widthPixels;

            BitmapFactory.Options BitmapFactoryOptionsbfo = new BitmapFactory.Options();
            BitmapFactoryOptionsbfo.inPreferredConfig = Bitmap.Config.RGB_565;
            imagePath = new ImagePath();
            position = imagePath.getPosition();
            Toast.makeText(SelectHairStyle.this,"dsfsaf",Toast.LENGTH_LONG).show();
            checkposition(position);

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inMutable = true;

            initializeBitmap(bitmapOptions);

            canvas = new Canvas(temporaryBitmap);
            canvas.drawBitmap(defaultBitmap, 0, 0, null);

            FaceDetector faceDetector = new com.google.android.gms.vision.face.FaceDetector.Builder(this)
                    .setTrackingEnabled(false)
                    .setLandmarkType(com.google.android.gms.vision.face.FaceDetector.ALL_LANDMARKS)
                    .build();

            if (!faceDetector.isOperational()) {
                new AlertDialog.Builder(this)
                        .setMessage("Face Detector could not be set up on your device :(")
                        .show();
            } else {
                Frame frame = new Frame.Builder().setBitmap(defaultBitmap).build();
                SparseArray<Face> sparseArray = faceDetector.detect(frame);

                detectFaces(sparseArray);

                image.setImageDrawable(new BitmapDrawable(getResources(), defaultBitmap));


                faceDetector.release();
            }
            final ActionBar abar = getSupportActionBar();
            View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar, null);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER);
            TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);

            textviewTitle.setText("TRY A HAIR STYLE LOOK");
            abar.setCustomView(viewActionBar, params);
            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            abar.setDisplayHomeAsUpEnabled(true);

            abar.setHomeButtonEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
            upArrow.setColorFilter(getResources().getColor(R.color.colorwhite), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            female.setOnClickListener(femaleclick);
            male.setOnClickListener(maleclick);
            female.setBackgroundResource(R.drawable.female1);
            if (position == 0) {

                image.setImageResource(0);

                image.destroyDrawingCache();
                //image.setImageBitmap(BitmapFactory.decodeFile(imagePath.getPath()));

                image.setImageBitmap(cameraimage);
                Log.d("cameraimage", "cameraimage");
//            if (myBitmap != null) {
//                image.buildDrawingCache();
//                myBitmap.recycle();
//                myBitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
//                new SingleTouchEventView(this);
//           }
// else {
//                image.buildDrawingCache();
//
//                myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.modellook44, BitmapFactoryOptionsbfo);
//                new SingleTouchEventView(this);
//            }
            }
            if (position == 1) {
                image.setImageResource(0);
                image.setImageBitmap(null);
                image.destroyDrawingCache();
                // image.setImageBitmap(BitmapFactory.decodeFile(imagePath.getPath()));
               /*image.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                R.drawable.modellook22, width, height));*/
                image.setImageBitmap(BitmapFactory.decodeFile(galeryimage));
                Log.d("galleryimage", "galleryimage");
            }
            if (position == 2) {
                image.setImageResource(0);
                image.setImageBitmap(null);
                image.destroyDrawingCache();
      /*  image.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                R.drawable.modellook33, width, height));*/
                image.setImageResource(R.drawable.modellook33);
            }

            classic_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!(classic_t)) {
                        classic_t = true;

                        moedern_type.setVisibility(View.GONE);
                        classic_hair.setVisibility(View.VISIBLE);
                    } else {
                        classic_t = false;

                        moedern_type.setVisibility(View.VISIBLE);
                        classic_hair.setVisibility(View.GONE);
                    }

                }
            });

            moedern_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!(modern_t)) {
                        classic_t = true;

                        classic_type.setVisibility(View.GONE);
                        modern_hair.setVisibility(View.VISIBLE);
                    } else {
                        modern_t = false;

                        classic_type.setVisibility(View.VISIBLE);
                        modern_hair.setVisibility(View.GONE);
                    }

                }
            });
            long_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!(long_t)) {
                        long_t = true;
                        short_type.setVisibility(View.GONE);
                        medium_type.setVisibility(View.GONE);
                        long_hair.setVisibility(View.VISIBLE);
                    } else {
                        long_t = false;
                        short_type.setVisibility(View.VISIBLE);
                        medium_type.setVisibility(View.VISIBLE);
                        long_hair.setVisibility(View.GONE);
                    }

                }
            });

            medium_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    short_type.setVisibility(View.GONE);
                    long_type.setVisibility(View.GONE);
                    medium_hair.setVisibility(View.VISIBLE);

                    if (!medium_t) {

                        short_type.setVisibility(View.GONE);
                        long_type.setVisibility(View.GONE);
                        medium_hair.setVisibility(View.VISIBLE);
                        medium_t = true;
                    } else {

                        short_type.setVisibility(View.VISIBLE);
                        long_type.setVisibility(View.VISIBLE);
                        medium_hair.setVisibility(View.GONE);
                        medium_t = false;
                    }
                }
            });
            short_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    long_type.setVisibility(View.GONE);
                    medium_type.setVisibility(View.GONE);
                    short_hair.setVisibility(View.VISIBLE);

                    if (!short_t) {
                        short_t = true;
                    } else {
                        short_t = false;
                        long_type.setVisibility(View.VISIBLE);
                        medium_type.setVisibility(View.VISIBLE);
                        short_hair.setVisibility(View.GONE);
                    }
                }
            });


        }

        private void checkposition(int position) {

        }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.sidemenu, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(SelectHairStyle.this, R.anim.slidein);
            //noinspection SimplifiableIfStatement
            if (id == R.id.action_sidemenu) {

                if (!side) {
                    side = true;
                    relativeLayout.startAnimation(hyperspaceJumpAnimation);
                    relativeLayout.setVisibility(View.VISIBLE);

                    malehair.setVisibility(View.GONE);
                    femalehair.setVisibility(View.GONE);
                } else {
                    side = false;
                    relativeLayout.setVisibility(View.GONE);
                }
            }
            return super.onOptionsItemSelected(item);
        }



        public void Initializer() {


            fl1 = (ImageView) findViewById(R.id.fl1);
            fl2 = (ImageView) findViewById(R.id.fl2);
            fl3 = (ImageView) findViewById(R.id.fl3);
            fl4 = (ImageView) findViewById(R.id.fl4);
            fl5 = (ImageView) findViewById(R.id.fl5);
            fl6 = (ImageView) findViewById(R.id.fl6);
            fl7 = (ImageView) findViewById(R.id.fl7);
            fl8 = (ImageView) findViewById(R.id.fl8);
            fl9 = (ImageView) findViewById(R.id.fl9);
            fl10 = (ImageView) findViewById(R.id.fl10);
            fl11 = (ImageView) findViewById(R.id.fl11);
            fl12 = (ImageView) findViewById(R.id.fl12);
            fl13 = (ImageView) findViewById(R.id.fl13);
            fl14 = (ImageView) findViewById(R.id.fl14);
            fl15 = (ImageView) findViewById(R.id.fl15);
            fl16 = (ImageView) findViewById(R.id.fl16);
            fl17 = (ImageView) findViewById(R.id.fl17);

            fm1 = (ImageView) findViewById(R.id.fm1);
            fm2 = (ImageView) findViewById(R.id.fm2);
            fm3 = (ImageView) findViewById(R.id.fm3);
            fm4 = (ImageView) findViewById(R.id.fm4);
            fm5 = (ImageView) findViewById(R.id.fm5);
            fm6 = (ImageView) findViewById(R.id.fm6);
            fm7 = (ImageView) findViewById(R.id.fm7);
            fm8 = (ImageView) findViewById(R.id.fm8);
            fm9 = (ImageView) findViewById(R.id.fm9);
            fm10 = (ImageView) findViewById(R.id.fm10);
            fm11 = (ImageView) findViewById(R.id.fm11);
            fm12 = (ImageView) findViewById(R.id.fm12);
            fm13 = (ImageView) findViewById(R.id.fm13);
            fm14 = (ImageView) findViewById(R.id.fm14);
            fm15 = (ImageView) findViewById(R.id.fm15);
            fm16 = (ImageView) findViewById(R.id.fm16);
            fm17 = (ImageView) findViewById(R.id.fm17);
            fm18 = (ImageView) findViewById(R.id.fm18);
            fm19 = (ImageView) findViewById(R.id.fm19);
            fm20 = (ImageView) findViewById(R.id.fm20);

            fm21 = (ImageView) findViewById(R.id.fm21);
            fm22 = (ImageView) findViewById(R.id.fm22);
            fm23 = (ImageView) findViewById(R.id.fm23);
            fm24 = (ImageView) findViewById(R.id.fm24);
            fm25 = (ImageView) findViewById(R.id.fm25);
            fm26 = (ImageView) findViewById(R.id.fm26);
            fm27 = (ImageView) findViewById(R.id.fm27);
            fm28 = (ImageView) findViewById(R.id.fm28);
            fm29 = (ImageView) findViewById(R.id.fm29);
            fm30 = (ImageView) findViewById(R.id.fm30);


            mc1 = (ImageView) findViewById(R.id.mc1);
            mc2 = (ImageView) findViewById(R.id.mc2);
            mc3 = (ImageView) findViewById(R.id.mc3);
            mc4 = (ImageView) findViewById(R.id.mc4);
            mc5 = (ImageView) findViewById(R.id.mc5);
            mc6 = (ImageView) findViewById(R.id.mc6);
            mc7 = (ImageView) findViewById(R.id.mc7);
            mc8 = (ImageView) findViewById(R.id.mc8);
            mc9 = (ImageView) findViewById(R.id.mc9);
            mc10 = (ImageView) findViewById(R.id.mc10);
            mc11 = (ImageView) findViewById(R.id.mc11);
            mc12 = (ImageView) findViewById(R.id.mc12);
            mc13 = (ImageView) findViewById(R.id.mc13);
            mc14 = (ImageView) findViewById(R.id.mc14);
            mc15 = (ImageView) findViewById(R.id.mc15);
            mc16 = (ImageView) findViewById(R.id.mc16);
            mc17 = (ImageView) findViewById(R.id.mc17);
            mc18 = (ImageView) findViewById(R.id.mc18);
            mc19 = (ImageView) findViewById(R.id.mc19);
            mc20 = (ImageView) findViewById(R.id.mc20);

            mm1 = (ImageView) findViewById(R.id.mm1);
            mm2 = (ImageView) findViewById(R.id.mm2);
            mm3 = (ImageView) findViewById(R.id.mm3);
            mm4 = (ImageView) findViewById(R.id.mm4);
            mm5 = (ImageView) findViewById(R.id.mm5);
            mm6 = (ImageView) findViewById(R.id.mm6);
            mm7 = (ImageView) findViewById(R.id.mm7);
            mm8 = (ImageView) findViewById(R.id.mm8);
            mm9 = (ImageView) findViewById(R.id.mm9);
            mm10 = (ImageView) findViewById(R.id.mm10);
            mm11 = (ImageView) findViewById(R.id.mm11);
            mm12 = (ImageView) findViewById(R.id.mm12);
            mm13 = (ImageView) findViewById(R.id.mm13);
            mm14 = (ImageView) findViewById(R.id.mm14);
            mm15 = (ImageView) findViewById(R.id.mm15);
            mm16 = (ImageView) findViewById(R.id.mm16);
            mm17 = (ImageView) findViewById(R.id.mm17);
            mm18 = (ImageView) findViewById(R.id.mm18);
            mm19 = (ImageView) findViewById(R.id.mm19);
            mm20 = (ImageView) findViewById(R.id.mm20);
            mm21 = (ImageView) findViewById(R.id.mm21);
            mm22 = (ImageView) findViewById(R.id.mm22);
            mm23 = (ImageView) findViewById(R.id.mm23);
            mm24 = (ImageView) findViewById(R.id.mm24);
            mm25 = (ImageView) findViewById(R.id.mm25);


        public void Initializer(){


            fl1=(ImageView)findViewById(R.id.fl1);
            fl2=(ImageView)findViewById(R.id.fl2);
            fl3=(ImageView)findViewById(R.id.fl3);
            fl4=(ImageView)findViewById(R.id.fl4);
            fl5=(ImageView)findViewById(R.id.fl5);
            fl6=(ImageView)findViewById(R.id.fl6);
            fl7=(ImageView)findViewById(R.id.fl7);
            fl8=(ImageView)findViewById(R.id.fl8);
            fl9=(ImageView)findViewById(R.id.fl9);
            fl10=(ImageView)findViewById(R.id.fl10);
            fl11=(ImageView)findViewById(R.id.fl11);
            fl12=(ImageView)findViewById(R.id.fl12);
            fl13=(ImageView)findViewById(R.id.fl13);
            fl14=(ImageView)findViewById(R.id.fl14);
            fl15=(ImageView)findViewById(R.id.fl15);
            fl16=(ImageView)findViewById(R.id.fl16);
            fl17=(ImageView)findViewById(R.id.fl17);

            fm1=(ImageView)findViewById(R.id.fm1);
            fm2=(ImageView)findViewById(R.id.fm2);
            fm3=(ImageView)findViewById(R.id.fm3);
            fm4=(ImageView)findViewById(R.id.fm4);
            fm5=(ImageView)findViewById(R.id.fm5);
            fm6=(ImageView)findViewById(R.id.fm6);
            fm7=(ImageView)findViewById(R.id.fm7);
            fm8=(ImageView)findViewById(R.id.fm8);
            fm9=(ImageView)findViewById(R.id.fm9);
            fm10=(ImageView)findViewById(R.id.fm10);
            fm11=(ImageView)findViewById(R.id.fm11);
            fm12=(ImageView)findViewById(R.id.fm12);
            fm13=(ImageView)findViewById(R.id.fm13);
            fm14=(ImageView)findViewById(R.id.fm14);
            fm15=(ImageView)findViewById(R.id.fm15);
            fm16=(ImageView)findViewById(R.id.fm16);
            fm17=(ImageView)findViewById(R.id.fm17);
            fm18=(ImageView)findViewById(R.id.fm18);
            fm19=(ImageView)findViewById(R.id.fm19);
            fm20=(ImageView)findViewById(R.id.fm20);

            fm21=(ImageView)findViewById(R.id.fm21);
            fm22=(ImageView)findViewById(R.id.fm22);
            fm23=(ImageView)findViewById(R.id.fm23);
            fm24=(ImageView)findViewById(R.id.fm24);
            fm25=(ImageView)findViewById(R.id.fm25);
            fm26=(ImageView)findViewById(R.id.fm26);
            fm27=(ImageView)findViewById(R.id.fm27);
            fm28=(ImageView)findViewById(R.id.fm28);
            fm29=(ImageView)findViewById(R.id.fm29);
            fm30=(ImageView)findViewById(R.id.fm30);

            fs1=(ImageView)findViewById(R.id.fs1);
            fs2=(ImageView)findViewById(R.id.fs2);
            fs3=(ImageView)findViewById(R.id.fs3);
            fs4=(ImageView)findViewById(R.id.fs4);
            fs5=(ImageView)findViewById(R.id.fs5);
            fs6=(ImageView)findViewById(R.id.fs6);
            fs7=(ImageView)findViewById(R.id.fs7);
            fs8=(ImageView)findViewById(R.id.fs8);
            fs9=(ImageView)findViewById(R.id.fs9);
            fs10=(ImageView)findViewById(R.id.fs10);
            fs11=(ImageView)findViewById(R.id.fs11);
            fs12=(ImageView)findViewById(R.id.fs12);
            fs13=(ImageView)findViewById(R.id.fs13);
            fs14=(ImageView)findViewById(R.id.fs14);
            fs15=(ImageView)findViewById(R.id.fs15);
            fs16=(ImageView)findViewById(R.id.fs16);
            fs17=(ImageView)findViewById(R.id.fs17);
            fs18=(ImageView)findViewById(R.id.fs18);
            fs19=(ImageView)findViewById(R.id.fs19);
            fs20=(ImageView)findViewById(R.id.fs20);
            fs21=(ImageView)findViewById(R.id.fs21);

            mc1=(ImageView)findViewById(R.id.mc1);
            mc2=(ImageView)findViewById(R.id.mc2);
            mc3=(ImageView)findViewById(R.id.mc3);
            mc4=(ImageView)findViewById(R.id.mc4);
            mc5=(ImageView)findViewById(R.id.mc5);
            mc6=(ImageView)findViewById(R.id.mc6);
            mc7=(ImageView)findViewById(R.id.mc7);
            mc8=(ImageView)findViewById(R.id.mc8);
            mc9=(ImageView)findViewById(R.id.mc9);
            mc10=(ImageView)findViewById(R.id.mc10);
            mc11=(ImageView)findViewById(R.id.mc11);
            mc12=(ImageView)findViewById(R.id.mc12);
            mc13=(ImageView)findViewById(R.id.mc13);
            mc14=(ImageView)findViewById(R.id.mc14);
            mc15=(ImageView)findViewById(R.id.mc15);
            mc16=(ImageView)findViewById(R.id.mc16);
            mc17=(ImageView)findViewById(R.id.mc17);
            mc18=(ImageView)findViewById(R.id.mc18);
            mc19=(ImageView)findViewById(R.id.mc19);
            mc20=(ImageView)findViewById(R.id.mc20);

            mm1=(ImageView)findViewById(R.id.mm1);
            mm2=(ImageView)findViewById(R.id.mm2);
            mm3=(ImageView)findViewById(R.id.mm3);
            mm4=(ImageView)findViewById(R.id.mm4);
            mm5=(ImageView)findViewById(R.id.mm5);
            mm6=(ImageView)findViewById(R.id.mm6);
            mm7=(ImageView)findViewById(R.id.mm7);
            mm8=(ImageView)findViewById(R.id.mm8);
            mm9=(ImageView)findViewById(R.id.mm9);
            mm10=(ImageView)findViewById(R.id.mm10);
            mm11=(ImageView)findViewById(R.id.mm11);
            mm12=(ImageView)findViewById(R.id.mm12);
            mm13=(ImageView)findViewById(R.id.mm13);
            mm14=(ImageView)findViewById(R.id.mm14);
            mm15=(ImageView)findViewById(R.id.mm15);
            mm16=(ImageView)findViewById(R.id.mm16);
            mm17=(ImageView)findViewById(R.id.mm17);
            mm18=(ImageView)findViewById(R.id.mm18);
            mm19=(ImageView)findViewById(R.id.mm19);
            mm20=(ImageView)findViewById(R.id.mm20);
            mm21=(ImageView)findViewById(R.id.mm21);
            mm22=(ImageView)findViewById(R.id.mm22);
            mm23=(ImageView)findViewById(R.id.mm23);
            mm24=(ImageView)findViewById(R.id.mm24);
            mm25=(ImageView)findViewById(R.id.mm25);


        }

        public void imageconversion() {
            fl1.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl1, 30, 30));
            fl2.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl2, 30, 30));
            fl3.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl3, 30, 30));
            fl4.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl4, 30, 30));
            fl5.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl5, 30, 30));
            fl6.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl6, 30, 30));
            fl7.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl7, 30, 30));
            fl8.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl8, 30, 30));
            fl9.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl9, 30, 30));

            fl10.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl10, 30, 30));


            fl11.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl11, 30, 30));
            fl12.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl12, 30, 30));
            fl13.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl13, 30, 30));
            fl14.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl14, 30, 30));
            fl15.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl15, 30, 30));
            fl16.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl16, 30, 30));
            fl17.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fl17, 30, 30));


            fm1.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm1, 30, 30));
            fm2.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm2, 30, 30));
            fm3.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm3, 30, 30));
            fm4.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm4, 30, 30));
            fm5.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm5, 30, 30));
            fm6.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm6, 30, 30));
            fm7.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm7, 30, 30));
            fm8.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm8, 30, 30));
            fm9.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm9, 30, 30));

            fm10.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm10, 30, 30));


            fm21.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm21, 30, 30));
            fm22.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm22, 30, 30));
            fm23.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm23, 30, 30));
            fm24.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm24, 30, 30));
            fm25.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm25, 30, 30));
            fm26.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm26, 30, 30));
            fm27.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm27, 30, 30));
            fm28.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm28, 30, 30));
            fm29.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm29, 30, 30));

            fm30.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm30, 30, 30));

            fm11.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm11, 30, 30));
            fm12.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm12, 30, 30));
            fm13.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm13, 30, 30));
            fm14.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm14, 30, 30));
            fm15.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm15, 30, 30));
            fm16.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm16, 30, 30));
            fm17.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm17, 30, 30));
            fm18.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm18, 30, 30));
            fm19.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm19, 30, 30));

            fm20.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fm20, 30, 30));


            fs1.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.drawable.fs1, 30, 30));
            fs2.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.drawable.fs2, 30, 30));
            fs3.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.drawable.fs3, 30, 30));
            fs4.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.drawable.fs4, 30, 30));
            fs5.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.drawable.fs5, 30, 30));
            fs6.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.drawable.fs6, 30, 30));
            fs7.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs7, 30, 30));
            fs8.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs8, 30, 30));
            fs9.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs9, 30, 30));

            fs10.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs10, 30, 30));

            fs11.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs11, 30, 30));
            fs12.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs12, 30, 30));
            fs13.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs13, 30, 30));
            fs14.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs14, 30, 30));
            fs15.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs15, 30, 30));
            fs16.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs16, 30, 30));
            fs17.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs17, 30, 30));
            fs18.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs18, 30, 30));
            fs19.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs19, 30, 30));

            fs20.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs20, 30, 30));
            fs21.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.fs21, 30, 30));

            mc1.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc1, 30, 30));
            mc2.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc2, 30, 30));
            mc3.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc3, 30, 30));
            mc4.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc4, 30, 30));
            mc5.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc5, 30, 30));
            mc6.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc6, 30, 30));
            mc7.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc7, 30, 30));
            mc8.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc8, 30, 30));
            mc9.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc9, 30, 30));

            mc10.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc10, 30, 30));


            mc11.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc11, 30, 30));
            mc12.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc12, 30, 30));
            mc13.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc13, 30, 30));
            mc14.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc14, 30, 30));
            mc15.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc15, 30, 30));
            mc16.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc16, 30, 30));
            mc17.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc17, 30, 30));
            mc18.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc18, 30, 30));
            mc19.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc19, 30, 30));

            mc20.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mc20, 30, 30));

            mm1.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm1, 30, 30));
            mm2.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm2, 30, 30));
            mm3.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm3, 30, 30));
            mm4.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm4, 30, 30));
            mm5.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm5, 30, 30));
            mm6.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm6, 30, 30));
            mm7.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm7, 30, 30));
            mm8.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm8, 30, 30));
            mm9.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm9, 30, 30));

            mm10.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm10, 30, 30));


            mm11.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm11, 30, 30));
            mm12.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm12, 30, 30));
            mm13.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm13, 30, 30));
            mm14.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm14, 30, 30));
            mm15.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm15, 30, 30));
            mm16.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm16, 30, 30));
            mm17.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm17, 30, 30));
            mm18.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm18, 30, 30));
            mm19.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm19, 30, 30));

            mm20.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm20, 30, 30));

            mm21.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm21, 30, 30));
            mm22.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm22, 30, 30));
            mm23.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm23, 30, 30));
            mm24.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm24, 30, 30));
            mm25.setImageBitmap(Imageresize.decodeSampledBitmapFromResource(getResources(),
                    R.mipmap.mm25, 30, 30));


        }


        public class SingleTouchEventView extends View {
            List<Point> points = new ArrayList<Point>();
            float myEyesDistance;
            int numberOfFaceDetected;
            SparseArray<Face> faceArray;
            private Paint paint = new Paint();
            private Paint paint1 = new Paint();
            private int imageWidth, imageHeight;
            private int numberOfFace = 5;
            private FaceDetector myFaceDetect;
            private FaceDetector.Face[] myFace;

            public SingleTouchEventView(Context context) {
                super(context);


                imageWidth = myBitmap.getWidth();
                imageHeight = myBitmap.getHeight();
                myFace = new FaceDetector.Face[numberOfFace];
                myFaceDetect = new FaceDetector(imageWidth, imageHeight, numberOfFace);
                numberOfFaceDetected = myFaceDetect.findFaces(myBitmap, myFace);

                com.google.android.gms.vision.face.FaceDetector detector = new com.google.android.gms.vision.face.FaceDetector.Builder(SelectHairStyle.this)
                        .setMode(com.google.android.gms.vision.face.FaceDetector.ACCURATE_MODE)
                        .setLandmarkType(com.google.android.gms.vision.face.FaceDetector.ALL_LANDMARKS)
                        .setClassificationType(com.google.android.gms.vision.face.FaceDetector.ALL_CLASSIFICATIONS)
                        .setTrackingEnabled(false).build();
                Frame frame = new Frame.Builder().setBitmap(myBitmap).build();
                faceArray = detector.detect(frame);

            public void clicklistner() {
                fs1.setOnClickListener(this);
                fs2.setOnClickListener(this);
            }

            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.fs1:
                        // do something
                        Log.d("hai--", "fs1");
                        break;
                    case R.id.fs2:
                        // do something else
                        Log.d("hai--", "fs2");
                        break;

                }
            }

            private void initializeBitmap(BitmapFactory.Options bitmapOptions) {


                if (position == 0) {
                    if (defaultBitmap != null) {
                        defaultBitmap.recycle();
                        defaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.modellook11,
                                bitmapOptions);
                    } else {

                        defaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.modellook11,
                                bitmapOptions);
                    }
                } else if (position == 1) {
                    if (defaultBitmap != null) {
                        defaultBitmap.recycle();
                        defaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.modellook22,
                                bitmapOptions);
                    } else {

                        defaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.modellook22,
                                bitmapOptions);
                    }
                } else if (position == 2) {
                    if (defaultBitmap != null) {
                        defaultBitmap.recycle();
                        defaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.modellook33,
                                bitmapOptions);
                    } else {

                        defaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.modellook33,
                                bitmapOptions);
                    }
                } else {
                    if (defaultBitmap != null) {
                        defaultBitmap.recycle();
                        defaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.modellook44,
                                bitmapOptions);
                    } else {

                        defaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.modellook44,
                                bitmapOptions);

                    }
                }

                temporaryBitmap = Bitmap.createBitmap(defaultBitmap.getWidth(), defaultBitmap
                        .getHeight(), Bitmap.Config.RGB_565);
       /* defaultBitmap=decodeSampledBitmapFromResource(getResources(), R.drawable.male_model,
                400,450);*/

      /*  eyePatchBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.fs1,
                bitmapOptions);*/

                eyePatchBitmap = Imageresize.decodeSampledBitmapFromResource(getResources(), R.drawable.fs1,
                        155, 100);
            }

 /*   private void detectFaces(SparseArray<Face> sparseArray) {

        for (int i = 0; i < sparseArray.size(); i++) {
            Face face = sparseArray.valueAt(i);

            float left = (face.getPosition().x);
            float top = (face.getPosition().y);
            float right = left + face.getWidth();
            float bottom = right + face.getHeight();
            float cornerRadius = 2.0f;

            RectF rectF = new RectF(left, top, right, bottom);
            //  canvas.drawRoundRect(rectF, cornerRadius, cornerRadius, rectPaint);
           canvas.drawBitmap(eyePatchBitmap, left , top , null);
            //detectLandmarks(face);
        }*/


            private void createRectanglePaint() {
                rectPaint = new Paint();
                rectPaint.setStrokeWidth(5);

                rectPaint.setColor(Color.CYAN);
                rectPaint.setStyle(Paint.Style.STROKE);
            }

            private void detectFaces(SparseArray<Face> sparseArray) {

                for (int i = 0; i < sparseArray.size(); i++) {
                    Face face = sparseArray.valueAt(i);

                    float left = (face.getPosition().x);
                    float top = (face.getPosition().y);
                    float right = left + face.getWidth();
                    float bottom = right + face.getHeight();
                    float cornerRadius = 2.0f;

                    RectF rectF = new RectF(left, top, right, bottom);
                    //   canvas.drawRoundRect(rectF, cornerRadius, cornerRadius, rectPaint);
                    canvas.drawBitmap(eyePatchBitmap, left, top, null);
                    detectLandmarks(face);

                }
            }

            private void detectLandmarks(Face face) {
                for (Landmark landmark : face.getLandmarks()) {

                    int cx = (int) (landmark.getPosition().x);
                    int cy = (int) (landmark.getPosition().y);


                    // canvas.drawCircle(cx, cy, 10, rectPaint);

                    // drawLandmarkType(landmark.getType(), cx, cy);
                    Log.d("landmark.getType()----", "" + landmark.getType());
                    //  drawEyePatchBitmap(landmark.getType(), cx, cy);
                }
            }

            private void drawLandmarkType(int landmarkType, float cx, float cy) {
                String type = String.valueOf(landmarkType);
                rectPaint.setTextSize(50);
                canvas.drawText(type, cx, cy, rectPaint);

            }

            private void drawEyePatchBitmap(int landmarkType, float cx, float cy) {

                if (landmarkType == 4) {
                    // TODO: Optimize so that this calculation is not done for every face
                    int scaledWidth = eyePatchBitmap.getScaledWidth(canvas);
                    int scaledHeight = eyePatchBitmap.getScaledHeight(canvas);
                    Log.d("scaledWidth----", "" + scaledWidth);
                    Log.d("scaledHeight----", "" + scaledHeight);
                    canvas.drawBitmap(eyePatchBitmap, cx - (scaledWidth / 2), cy - (scaledHeight / 2), null);
                }
            }
        }
