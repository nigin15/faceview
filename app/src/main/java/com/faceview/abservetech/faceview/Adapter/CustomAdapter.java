package com.faceview.abservetech.faceview.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.faceview.abservetech.faceview.R;
import com.faceview.abservetech.faceview.SelectModel;

/**
 * Created by ABSERVETECH on 16-Sep-16.
 */
public class CustomAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;

    Context context;
    int[] imageId;

    public CustomAdapter(SelectModel mainActivity, int[] prgmImages) {
        // TODO Auto-generated constructor stub

        context = mainActivity;
        imageId = prgmImages;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return imageId.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.plotlist, null);
        rowView.setPadding(10, 10, 10, 10);
        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);


        holder.img = (ImageView) rowView.findViewById(R.id.imageView1);
        rowView.startAnimation(hyperspaceJumpAnimation);

        holder.img.setImageResource(imageId[position]);

        rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


//
            }
        });


        return rowView;
    }

    public class Holder {
        TextView tv;
        ImageView img;
    }

}